#Author: h.andresc1127@gmail.com
@lcnIdiomas
Feature: Reservacion de clases LCN Idiomas
  COMO Un estudiante matriculado en LCN Idiomas
  QUIERO Realizar la reservaciones de mis clases
  PARA Tener el proceso automatizado y no hacerlo manual

  Scenario: Reservar clases
    Given Estoy en la URL "https://usuarios.lcnidiomas.edu.co/login"
    And Ingreso "1128435672" en el campo "Username"
    And Ingreso "1128435672" en el campo "Password"
    And Doy clic en "Iniciar Sesion"
    And Doy clic en "Reservar Clase Ahora" forzado
    And Doy clic en "Sede Santa Fé" forzado
    And Guardo el atributo "value" de "id matricula" como "id_matricula"
    And Guardo el atributo "innerHTML" de "script" como "script clase"
    And Analizar "script clase"
    
