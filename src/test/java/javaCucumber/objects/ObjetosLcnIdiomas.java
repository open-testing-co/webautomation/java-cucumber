package javaCucumber.objects;

import org.openqa.selenium.By;

import javaCucumber.utilities.ActionsUtil;

public class ObjetosLcnIdiomas {
	public ObjetosLcnIdiomas() {
		ActionsUtil.objetosPut("username",     			By.name("user"));
		ActionsUtil.objetosPut("password",     			By.name("pass"));
		ActionsUtil.objetosPut("iniciarsesion",     	By.cssSelector(".btn-info"));
		ActionsUtil.objetosPut("reservarclaseahora",    By.xpath("(//*[@href='https://usuarios.lcnidiomas.edu.co/estudiantes/reservar-clase'])[1]"));
		ActionsUtil.objetosPut("sedesantafe",    		By.id("Sede_45"));
		ActionsUtil.objetosPut("idmatricula",    		By.id("matricula_id"));
		
	}

}
