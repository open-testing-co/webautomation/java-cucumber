package javaCucumber.utilities;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;


public class DriverManagerFactory {
	static WebDriver driver;
	static PropertiesLoader properties = PropertiesLoader.getInstance();
	private static final Logger LOGGER = LoggerFactory.getLogger(DriverManagerFactory.class);

	public DriverManagerFactory() {
		String driverType = properties.getProperty("webdriver.driver");
		switch (ActionsUtil.textoMinusculasSinEspacios(driverType)) {
		case "firefox":
			String firefoxDriverPath = properties.getProperty("webdriver.firefox.driver");
			System.setProperty("webdriver.chfirefoxrome.driver", firefoxDriverPath);
			driver = new FirefoxDriver();
			break;
		default:
			String chromeDriverPath = properties.getProperty("webdriver.chrome.driver");
			String chromeSwitches = properties.getProperty("chrome.switches");
			String verbose = properties.getProperty("webdriver.chrome.verboseLogging");
			String logPath=System.getProperty("user.dir") + "\\target\\chromedriver.log";
			LOGGER.info(logPath);
			System.setProperty("webdriver.chrome.driver", chromeDriverPath);
			System.setProperty("webdriver.chrome.logfile", logPath);
			System.setProperty("webdriver.chrome.verboseLogging", verbose);
			ChromeOptions options = new ChromeOptions();
			options.addArguments(chromeSwitches);
			driver = new ChromeDriver(options);
			break;

		}
	}

	public static WebDriver getDriver() {
		return driver;
	}
	
	protected void finalize()   
    {   
        driver.quit(); 
    } 

}
