package javaCucumber.pages;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

import org.json.JSONArray;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javaCucumber.objects.DatosNegocio;
import javaCucumber.objects.ObjetosLcnIdiomas;
import javaCucumber.utilities.ActionsUtil;
import javaCucumber.utilities.DriverManagerFactory;

public class PageDefault {

	static By objetoToAction = By.xpath("/html/body");
	private static final Logger LOGGER = LoggerFactory.getLogger(PageDefault.class);
	static WebDriver driver;
	DriverManagerFactory page;

	public PageDefault() {
		if (ActionsUtil.objetosIsEmpty()) {
			LOGGER.info("Inicialización de objetos");
			page = new DriverManagerFactory();
			new ObjetosLcnIdiomas();
		}
	}

	public By getObjetoToAction() {
		return objetoToAction;
	}

	public void setObjetoToAction(By objetoToCliked) {
		objetoToAction = objetoToCliked;
	}

	public void sharedObjet(String opcion) {
		String nombreObjeto = (ActionsUtil.textoMinusculasSinEspacios(opcion));
		By byObjeto = ActionsUtil.getObjeto(nombreObjeto);
		setObjetoToAction(byObjeto);
	}

	public void irA(String url) {
		driver = DriverManagerFactory.getDriver();
		String urlActualizada = ActionsUtil.updateUrlWithBaseUrlIfDefined(url);
		ActionsUtil.goToWebSide(driver, urlActualizada);
	}

	public void ingresarTexto(String objeto, String texto) {
		sharedObjet(objeto);
		ActionsUtil.setTextField(driver, getObjetoToAction(), texto);
	}

	public void clic(String objeto) {
		sharedObjet(objeto);
		ActionsUtil.clic(driver, getObjetoToAction());
	}

	public void clicForzado(String objeto) {
		sharedObjet(objeto);
		ActionsUtil.ejecutarScript(driver, "arguments[0].click();", getObjetoToAction());
	}

	public void obtenerGuardarString(String objeto, String atributo, String datokey) {
		sharedObjet(objeto);
		String datoValue = ActionsUtil.getAttribute(driver, getObjetoToAction(), atributo);
		if (datoValue.isEmpty()) {
			datoValue = ActionsUtil.getTextAttribute(driver, getObjetoToAction());
		}
		DatosNegocio.dataPut(datokey, datoValue);
		LOGGER.info("Se guardo el dato: \"" + datokey + "\" con el valor: \"" + datoValue + "\"");
	}
	
	public Object getValuesForGivenKey(String jsonArrayStr, String key) {
	    JSONArray jsonArray = new JSONArray(jsonArrayStr);
	    Object x=jsonArray.query("/0/b");
	    return x;
	}

	public void analizar(String script) {
		String archivo = DatosNegocio.dataGet(script);
		int i = archivo.indexOf("events: [ ");
		archivo = archivo.substring(i);
		i = archivo.indexOf("],");
		archivo = archivo.substring(8, i + 1);
		archivo = archivo.replace("'", "\"");
		archivo = archivo.replace("id:", "\"id\":");
		archivo = archivo.replace("title:", "\"title\":");
		archivo = archivo.replace("start:", "\"start\":");
		archivo = archivo.replace("end:", "\"end\":");
		archivo = archivo.replace("color:", "\"color\":");
		archivo = archivo.replace("textColor:", "\"textColor\":");
		archivo = archivo.replace("dia:", "\"dia\":");
		archivo = archivo.replace("horario:", "\"horario\":");
		archivo = archivo.replace("salon:", "\"salon\":");
		archivo = archivo.replace("profesor:", "\"profesor\":");
		archivo = archivo.replace("imagen:", "\"imagen\":");
		archivo = archivo.replace("salon_id", "\"salon_id\"");



		try {
			BufferedWriter writer = new BufferedWriter(new FileWriter("target\\script.txt"));
			writer.write(archivo);
			writer.close();
		} catch (IOException e) {
		}

	}

}
