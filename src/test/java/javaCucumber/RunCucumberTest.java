package javaCucumber;

import org.junit.runner.RunWith;
import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(
		features = "src/test/resources/javaCucumber",
		tags = { "" }, monochrome = true, 
		glue={
				"javaCucumber"
				},
		plugin = { 
				"pretty", "html:target/reports/html/",
				"junit:target/reports/junit.xml", 
				"junit:target/reports/test.xml", 
				"junit:target/reports/TEST-all.xml",
				"json:target/reports/cukes.json" 
				}
		)

public class RunCucumberTest {
}
