package javaCucumber.definitions;

import cucumber.api.java.en.Given;
import javaCucumber.pages.PageDefault;

public class Stepdefs {
	
	PageDefault paginaDefault;

	@Given("Estoy en la URL {string}")
	public void estoyEnLaUrl(String url) {
		paginaDefault = new PageDefault();
		paginaDefault.irA(url);
	}

	@Given("Ingreso {string} en el campo {string}")
	public void ingresoEnElCampo(String texto, String objeto) {
		paginaDefault.ingresarTexto(objeto,texto);
	}

	@Given("Doy clic en {string}")
	public void doyClicEn(String objeto) {
	    paginaDefault.clic(objeto);
	}
	
	@Given("Doy clic en {string} forzado")
	public void doyClicEnForzado(String objeto) {
	    paginaDefault.clicForzado(objeto);
	}

	@Given("Guardo el atributo {string} de {string} como {string}")
	public void guardo_el_atributo_de_como(String atributo, String objeto, String guardar) {
		paginaDefault.obtenerGuardarString(objeto, atributo, guardar);
	}
	
	@Given("Analizar {string}")
	public void analizar(String script) {
	    paginaDefault.analizar(script);
	}

}
